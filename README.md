# Scripts to use with ML tracking software on the tensorbook

## Starting up the latest setup

```bash
(base) paul@tensorbook:~/scripts$ ./run_ml_july_2020
(base) paul@tensorbook:~/scripts$ ./run_python_sync
(base) paul@tensorbook:~/scripts$ ./run_boxview
```

## Notes

. ```run_python_sync``` starts two python processes in the background. If you want to restart these you need to kill those processes first

. When finished or before closing the lid on the tempsorbook, make sure to stop the ML docker containers vi ```./stop_containers```

. ```run_boxview``` will hog the terminal unless you postfix an '&' to put it into the background
